# -*- coding: utf-8 -*-
"""
Created on Mon Sep 07 21:22:07 2015

@author: Ashish
"""

from lxml import html
import requests
import json
import os
import numpy as np
from numpy import arange
from parse import *
import parse
import re
from datetime import datetime
import time
import numpy as np
from json import dumps, load
from datetime import timedelta, date
from collections import OrderedDict
from bs4 import BeautifulSoup


#1 month limit FOR NOW
MAX_DAYS=31

#1 is the maximum number of seats one would search for
MAX_SEATS=2

for_comma = 0

#this dictionary os to get the latitude and longitude of the places
latlongdict = OrderedDict(
[('Amsterdam' ,    [52.370216  ,   4.895168]),

('Zurich' ,       [47.376887  ,   8.541694]),

('Paris' ,        [48.856614  ,   2.352222]),

('Milan' ,        [45.465422  ,   9.185924]),

('Brussel' ,      [50.85034   ,   4.35171]),

('Prague' ,       [50.075538  ,  14.4378]),

('Vienna' ,       [48.208174  ,  16.373819]),

('Warsaw' ,       [52.229676  ,  21.012229]),

('Berlin' ,       [52.520007  ,  13.404954]),

('Hamburg' ,      [53.551085  ,   9.993682]),

('Leipzig' ,      [51.339695  ,  12.373075]),

('Dresden' ,      [51.050409  ,  13.737262]),

('Nuremberg' ,    [49.45203  ,   11.07675]),

('Munich' ,       [48.135125  ,  11.581981]),

('Stuttgart' ,    [48.775846  ,   9.182932]),

('Frankfurt' ,    [50.110922  ,   8.682127]),

('Dusseldorf' ,   [51.227741  ,   6.773456]),

('Hannover' ,     [52.375892  ,   9.73201])]
)


#this dictionary is to facilitate the conversion from place names to their url portions in blablacar.de
connectdict = {'Hannover': ['Dusseldorf', 'Stuttgart', 'Frankfurt', 'Munich', 'Nuremberg', 'Dresden', 'Leipzig', 'Berlin',
                           'Hamburg', 'Warsaw', 'Prague', 'Vienna', 'Milan', 'Zurich', 'Paris', 'Brussel', 'Amsterdam'], 
               'Dusseldorf' : ['Stuttgart', 'Frankfurt', 'Munich', 'Nuremberg', 'Dresden', 'Leipzig', 'Berlin',
                           'Hamburg', 'Hannover', 'Warsaw', 'Prague', 'Vienna', 'Milan', 'Zurich', 'Paris', 'Brussel'], 
               'Frankfurt' : ['Dusseldorf', 'Stuttgart', 'Munich', 'Nuremberg', 'Dresden', 'Leipzig', 'Berlin',
                           'Hamburg', 'Hannover', 'Warsaw', 'Prague', 'Vienna', 'Milan', 'Zurich', 'Paris', 'Brussel'],
               'Stuttgart' : ['Dusseldorf', 'Frankfurt', 'Munich', 'Nuremberg', 'Dresden', 'Leipzig', 'Berlin',
                           'Hamburg', 'Hannover', 'Warsaw', 'Prague', 'Vienna', 'Milan', 'Zurich', 'Paris'], 
               'Munich' : ['Dusseldorf', 'Stuttgart', 'Frankfurt', 'Nuremberg', 'Dresden', 'Leipzig', 'Berlin',
                           'Hamburg', 'Hannover', 'Warsaw', 'Prague', 'Vienna', 'Zurich', 'Paris', 'Amsterdam'], 
               'Nuremberg' : ['Dusseldorf', 'Stuttgart', 'Frankfurt', 'Munich', 'Dresden', 'Leipzig', 'Berlin',
                           'Hamburg', 'Hannover', 'Warsaw', 'Prague', 'Vienna', 'Zurich', 'Paris', 'Brussel', 'Amsterdam'],
               'Dresden' : ['Dusseldorf', 'Stuttgart', 'Frankfurt', 'Munich', 'Nuremberg', 'Leipzig', 'Berlin',
                           'Hamburg', 'Hannover', 'Prague', 'Vienna',  'Zurich'], 
               'Leipzig' : ['Dusseldorf', 'Stuttgart', 'Frankfurt', 'Munich', 'Nuremberg', 'Dresden', 'Berlin',
                           'Hamburg', 'Hannover', 'Warsaw', 'Paris'], 
               'Berlin' : ['Dusseldorf', 'Stuttgart', 'Frankfurt', 'Munich', 'Nuremberg', 'Dresden', 'Leipzig',
                           'Hamburg', 'Hannover', 'Warsaw', 'Prague', 'Vienna', 'Milan', 'Zurich', 'Paris', 'Brussel', 'Amsterdam'], 
               'Hamburg' : ['Dusseldorf', 'Stuttgart', 'Frankfurt', 'Munich', 'Nuremberg', 'Dresden', 'Leipzig', 'Berlin',
                          'Hannover', 'Warsaw', 'Prague', 'Vienna', 'Milan', 'Zurich', 'Paris', 'Amsterdam'], 
               'Warsaw' : ['Dusseldorf', 'Stuttgart', 'Frankfurt', 'Munich', 'Nuremberg', 'Leipzig', 'Berlin',
                           'Hannover'], 
               'Prague' : ['Dusseldorf', 'Stuttgart', 'Frankfurt', 'Munich', 'Nuremberg', 'Dresden', 'Berlin',
                           'Hamburg', 'Hannover'],
               'Vienna' : ['Dusseldorf', 'Stuttgart', 'Frankfurt', 'Munich', 'Nuremberg', 'Dresden', 'Berlin',
                           'Hamburg', 'Hannover'], 
               'Milan' : ['Dusseldorf', 'Stuttgart', 'Frankfurt', 'Berlin',
                           'Zurich'],
               'Zurich' : ['Dusseldorf', 'Stuttgart', 'Frankfurt', 'Munich', 'Nuremberg', 'Dresden', 'Leipzig', 'Berlin',
                           'Hamburg', 'Hannover', 'Milan'],
               'Paris' : ['Dusseldorf', 'Stuttgart', 'Frankfurt', 'Munich', 'Nuremberg', 'Dresden', 'Leipzig', 'Berlin',
                           'Hamburg', 'Hannover'], 
               'Brussel' : ['Dusseldorf', 'Frankfurt', 'Nuremberg',  'Berlin',
                           'Hamburg', 'Hannover'],
               'Amsterdam' : ['Dresden', 'Berlin',
                           'Hamburg', 'Hannover']}


#this dictionary is to map each city with other CONNECTD cities via this postbus service
urldict = {'Hannover':'Hannover-ZOB-am-Hbf', 'Dusseldorf' : 'Duesseldorf-ZOB-am-Hbf', 'Frankfurt' : 'Frankfurt-am-Main',
                'Stuttgart' : 'Stuttgart', 'Munich' : 'Muenchen', 'Nuremberg' : 'Nuernberg',
                'Dresden' : 'Dresden-Hbf', 'Leipzig' : 'Leipzig', 'Berlin' : 'Berlin', 
                'Hamburg' : 'Hamburg-ZOB-am-Hbf', 'Warsaw' : 'Warschau-Zentral', 'Prague' : 'Prag-UAN-Florenc',
                'Vienna' : 'Wien-U3-Station-Erdberg', 'Milan' : 'Mailand-Autostazione-C4-Milano-Lamp',
                'Zurich' : 'Zuerich-Sihlquai-HB', 'Paris' : 'Paris-Gare-Int-de-Paris-Gallieni', 'Brussel' : 'Bruessel-Gare-du-Nord',
                'Amsterdam' : 'Amsterdam'}


#this dictionary is for German months to numbers
monthdict = {'Januar':1, 
             'Februar':2,
             'Marz':3,
             'M\xe4rz':3,
             'M\xc3\xa4rz':3,
             'April':4,
             'Mai':5,
             'Juni':6,
             'Juli':7,
             'August':8,
             'September':9,
             'Oktober':10,
             'November':11,
             'Dezember':12}
             
dayofweekdict = {'Montag':1,
                 'Dienstag':2,
                 'Mittwoch':3,
                 'Donnerstag':4,
                 'Freitag':5,
                 'Samstag':6,
                 'Sonntag':7}
                 

#OPEN A JSON FILE TO WRITE ALL THE CONTENTS ABOUT TO BE SCRAPED HERE
filepath = '/home/ubuntu/AARCTERN/git/aarctern-inter_commute-buses/postbus/data/' + time.strftime("%Y_%m_%d") + '.json'
fout = open(filepath,'wb')
#write the intial '[' to start the list
fout.write('[\n')



#search_query = 'https://www.postbus.de/en/Fahrkarten/Start/Muenchen/Muenchen-Berlin.html?origin=moCatOrigin_7004&destination=moCatDest_7001&departure=2015-11-09&adults=1&children=0#'

#page = requests.get(search_query)

#soup = BeautifulSoup(page.text)

#for i in s.find_all('a'):
#    if 'class' in i.attrs:
#             if ((i['class'][0] == 'day-selector__item') and (i['class'][1] == 'current')):
#                     gold = i.strong.span.string
#
#[gold is the requred data from the page i.e. the minimum price for which bus is available
#and unicode(gold) looks like u'\r\n                                19,00\r\n']
#
#gold = unicode(gold)  #to convert from BeautifulSoup's NavigableString object to unicode string
#gold = int(gold.split()[0].split(',')[0]) + float(gold.split()[0].split(',')[1])





#Format of each entry of the json data
#{"results":[],"sourceName":"Paris","sourceLatitude":"48.833333","sourceLongitude":"2.333333",
#"destinationname":"Paris","destinationLatitude":"48.833333","destinationLongitude":"2.333333",
#"searchDate":20,"searchMonth":9,"searchYear":2015,"searchDay":"Sunday","searchTimestamp":1442734693448,
#"travelDate":21,"travelMonth":9,"travelYear":2015,"travelDay":"Monday","travelTimestamp":1442773800000,
#"resultCount":0}

#location of the folder where all json data is to be stored

for fromcity in latlongdict.keys():
    for tocity in connectdict[fromcity]:
        
        #get the source and dest lat and long using dict
        sourceLatitude = latlongdict[fromcity][0]
        sourceLongitude = latlongdict[fromcity][1]
        destinationLatitude = latlongdict[tocity][0]
        destinationLongitude = latlongdict[tocity][1]
        
        #get the source and dest city names
        sourceName = fromcity
        destinationName = tocity
        
        #get the searching date and time
        searchdt = datetime.utcnow()
        searchDate = searchdt.day
        searchMonth = searchdt.month
        searchYear = searchdt.year
        searchDay = searchdt.isoweekday()
        zerotimestamp = datetime.fromtimestamp(0)
        searchTimestamp = int((searchdt - zerotimestamp).total_seconds())
        
        #RUN THE LOOP FOR SEARCHING FOR NEXT 6 MONTHS i.e. 180 days FROM NOW
        for day_counter in range(1,MAX_DAYS):
            
            #get the date to be inserted into the query
            traveldateobject = date.today() + timedelta(days = day_counter)
            traveldatestring = unicode(traveldateobject)
            travelDate = traveldateobject.day
            travelMonth = traveldateobject.month
            travelYear = traveldateobject.year
            travelDay = traveldateobject.isoweekday()
            zerotimestamp2 = date.fromtimestamp(0)
            travelTimestamp = int((traveldateobject - zerotimestamp2).total_seconds())
            
            #MAIN BUSINESS : GENERATE THE URL and GET RESULTS
            sourceUrl = urldict[fromcity]
            destinationUrl = urldict[tocity]
            
            #RUN ANOTHER LOOP FOR THE NUMBER OF SEATS WE ARE LOOKING FOR (from 1 to 4)
            for num_seats in range(1,MAX_SEATS):
                
                #FORMULATE THE SEARCH QUERY
                search_query = 'https://www.postbus.de/en/Fahrkarten/Start/' + sourceUrl + '/' + sourceUrl + '-' + destinationUrl + '.html?departure=' + traveldatestring + '&adults=' + unicode(num_seats) + '&children=0#outward-journey'

                page = requests.get(search_query)

                #SOUP EVERYTHING IN!!
                soup = BeautifulSoup(page.text)                
                #INITIALIZE AVAIL
                avail = 0
                #SEARCH FOR THE TODAY's MINIMUM PRICE IN HTML PAGE
                for i in soup.find_all('a'):
                    if 'class' in i.attrs:
                         if ((i['class'][0] == 'day-selector__item') and (i['class'][1] == 'current')):
                                price = i.strong.span.string
                                #to convert from BeautifulSoup's NavigableString object to unicode string
                                price = unicode(price)  
                                #check if the price obtained is not an empty string
                                if (price == 'None'):
                                    avail = 0
                                    price = 0                                   
                                elif (len(price.split()[0].split(',')[1]) > 1):
                                    #get the numerical value (in euros)
                                    price = int(price.split()[0].split(',')[0]) + float(price.split()[0].split(',')[1])
                                    avail = 1
                                else:
                                    price = int(price.split()[0].split(',')[0])
                                    avail = 1
               
                if (avail == 0):
                    #WE COULD ALSO PUT THE PRIC HERE AS NaN
                    price = 0
                    

        
                #=============WRITE THESE THINGS TO THE JSON FILE==============
                if (for_comma==1):
                    fout.write('\n,\n')
                json.dump(OrderedDict([("resultAvailability",avail), ("resultPrice",price), 
                ("seatsLeft" , num_seats), ("sourceName",sourceName),("sourceLatitude",sourceLatitude),
                ("sourceLongitude",sourceLongitude), ("destinationname",destinationName), 
                ("destinationLatitude",destinationLatitude),("destinationLongitude",destinationLongitude),
                ("searchDate",searchDate), ("searchMonth",searchMonth), ("searchYear",searchYear),
                ("searchDay",searchDay), ("searchTimestamp",searchTimestamp), ("travelDay", travelDay),
                ("travelMonth", travelMonth), ("travelDate", travelDate), ("travelYear", travelYear),
                ("travelTimestamp",travelTimestamp)]), fout)
                #also append a comma with nextlines
                for_comma = 1
                
                
#add the ending ']' to the json file
fout.write('\n]')
#FINALLY, CLOSE THE JSON FILE
fout.close()                