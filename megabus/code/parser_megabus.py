# -*- coding: utf-8 -*-
"""
Created on Mon Sep 07 21:22:07 2015

@author: Ashish
"""

from lxml import html
import requests
import json
import os
import numpy as np
from numpy import arange
from parse import *
import parse
import re
from datetime import datetime
import time
import numpy as np
from json import dumps, load
from datetime import timedelta, date
from collections import OrderedDict
from bs4 import BeautifulSoup

INF = 999999999999

#3 month limit FOR NOW because after 60-70 days availabilities are 0 in all cases
MAX_DAYS=91

#1 is the maximum number of seats one would search for
MAX_SEATS=2

for_comma = 0

#this dictionary os to get the latitude and longitude of the places
latlongdict = OrderedDict(
[('Amsterdam' ,    [52.370216  ,   4.895168]),

('Rome' ,         [41.902783  ,  12.496366]),

('Paris' ,        [48.856614  ,   2.352222]),

('Milan' ,        [45.465422  ,   9.185924]),

('Brussel' ,      [50.85034   ,   4.35171]),

('Luxembourg' ,   [49.611621  ,   6.131935]),

('Berlin' ,       [52.520007  ,  13.404954]),

('Hamburg' ,      [53.551085  ,   9.993682]),

('Leipzig' ,      [51.339695  ,  12.373075]),

('Nuremberg' ,    [49.45203  ,   11.07675]),

('Munich' ,       [48.135125  ,  11.581981]),

('Stuttgart' ,    [48.775846  ,   9.182932]),

('Frankfurt' ,    [50.110922  ,   8.682127]),

('Hannover' ,     [52.375892  ,   9.73201])]
)


#this dictionary is to facilitate the conversion from place names to their url portions in blablacar.de
connectdict = {'Hannover': ['Munich', 'Nuremberg', 'Berlin',
                           'Hamburg'],                 
               'Frankfurt' : ['Stuttgart', 'Munich', 'Paris', 'Brussel', 'Amsterdam'],
               'Stuttgart' : ['Amsterdam', 'Brussel', 'Frankfurt', 'Munich', 'Paris'], 
               'Munich' : ['Amsterdam', 'Berlin', 'Brussel', 'Frankfurt', 'Hamburg', 'Hannover', 'Leipzig', 'Nuremberg', 'Paris', 'Stuttgart'], 
               'Nuremberg' : ['Berlin', 'Hamburg', 'Hannover', 'Leipzig', 'Munich'],               
               'Leipzig' : ['Berlin', 'Munich', 'Nuremberg'], 
               'Berlin' : ['Hannover', 'Leipzig', 'Munich', 'Nuremberg'], 
               'Hamburg' : ['Hannover', 'Munich', 'Nuremberg'],                                             
               'Milan' : ['Paris', 'Rome'],               
               'Paris' : ['Amsterdam', 'Brussel', 'Frankfurt', 'Milan', 'Munich', 'Stuttgart'], 
               'Brussel' : ['Amsterdam', 'Frankfurt', 'Luxembourg', 'Paris', 'Stuttgart'],
               'Amsterdam' : ['Brussel', 'Frankfurt'],
               'Rome' : ['Milan'],
               'Luxembourg' : ['Brussel']}


#this dictionary is to map each city with other CONNECTD cities via this postbus service
urldict = {'Hannover':'194', 
                'Frankfurt' : '188',
                'Stuttgart' : '189', 
                'Munich' : '190', 
                'Nuremberg' : '199',
                'Leipzig' : '198', 
                'Berlin' : '197', 
                'Hamburg' : '191',
                'Milan' : '187',
                'Paris' : '113', 
                'Brussel' : '112',
                'Amsterdam' : '110', 
                'Luxembourg' : '184', 
                'Rome' : '186'}


#this dictionary is for German months to numbers
monthdict = {'Januar':1, 
             'Februar':2,
             'Marz':3,
             'M\xe4rz':3,
             'M\xc3\xa4rz':3,
             'April':4,
             'Mai':5,
             'Juni':6,
             'Juli':7,
             'August':8,
             'September':9,
             'Oktober':10,
             'November':11,
             'Dezember':12}
             
dayofweekdict = {'Montag':1,
                 'Dienstag':2,
                 'Mittwoch':3,
                 'Donnerstag':4,
                 'Freitag':5,
                 'Samstag':6,
                 'Sonntag':7}
                 

#OPEN A JSON FILE TO WRITE ALL THE CONTENTS ABOUT TO BE SCRAPED HERE
filepath = '/home/ubuntu/AARCTERN/git/aarctern-inter_commute-buses/megabus/data/' + time.strftime("%Y_%m_%d") + '.json'
fout = open(filepath,'wb')
#write the intial '[' to start the list
fout.write('[\n')


#=====================DATA-MINING-LOGIC-EXPLANATION=================================================================================

#search_query = 'http://uk.megabus.com/JourneyResults.aspx?originCode=190&destinationCode=197&outboundDepartureDate=12%2f11%2f2015&inboundDepartureDate=&passengerCount=1&transportType=-1'


#page = requests.get(search_query)

#soup = BeautifulSoup(page.text)

# for i in soup.find_all('ul'):
#    if 'id' in i.attrs:
#====id of requred fields looks like this===========================
#<ul id="JourneyResylts_OutboundList_GridViewResults_ctl01_row_item" #class="journey standard none">
#===================================================================
#             if (("JourneyResylts_OutboundList_GridViewResults" in i['id']) and ("row_item" in i['id'])):
#store the ul tag content as bs4 tag object and then further iterate on it
#                     ul_object = i
#========look for 'li' tag with class = 'five' like this:=========== 
#<li class="five">
#							
#							<p>
#								
#								
#
#								£11.00
#							</p>
#						</li>
#===================================================================
#                     for ul_iterator in ul_object.find_all('li'):
#                            if 'class' in ul_iterator.attrs:
#required ul_iterator['class'] looks like ['five'] (list of strings), thats why we need {"five" in ii['class'], NOT "five" == ii['class']}
#                                if ("five" in ul_iterator['class']):
#                                      li_object = ul_iterator
#li_object stores the amount in GBP as shown above
#                                       price_string = unicode(li_object.p.string).split()[0].encode('utf-8')
#price string looks like: '\xc2\xa311.00'
#                                       price_string = price_string.split('.')
#separate the number before and after decimal
#                                       pre_decimal = re.search(r"\d+",price_string[0])
#                                       post_decimal = re.search(r"\d+",price_string[1])
#                                       price_num = int(pre_decimal) + float(post_decimal)
#                                       if price_num < price:
#                                            price = price_num
#                                       avail = avail + 1
#===================================================================================================================================





#Format of each entry of the json data
#{"results":[],"sourceName":"Paris","sourceLatitude":"48.833333","sourceLongitude":"2.333333",
#"destinationname":"Paris","destinationLatitude":"48.833333","destinationLongitude":"2.333333",
#"searchDate":20,"searchMonth":9,"searchYear":2015,"searchDay":"Sunday","searchTimestamp":1442734693448,
#"travelDate":21,"travelMonth":9,"travelYear":2015,"travelDay":"Monday","travelTimestamp":1442773800000,
#"resultCount":0}

#location of the folder where all json data is to be stored

for fromcity in latlongdict.keys():
    for tocity in connectdict[fromcity]:
        
        #get the source and dest lat and long using dict
        sourceLatitude = latlongdict[fromcity][0]
        sourceLongitude = latlongdict[fromcity][1]
        destinationLatitude = latlongdict[tocity][0]
        destinationLongitude = latlongdict[tocity][1]
        
        #get the source and dest city names
        sourceName = fromcity
        destinationName = tocity
        
        #get the searching date and time
        searchdt = datetime.utcnow()
        searchDate = searchdt.day
        searchMonth = searchdt.month
        searchYear = searchdt.year
        searchDay = searchdt.isoweekday()
        zerotimestamp = datetime.fromtimestamp(0)
        searchTimestamp = int((searchdt - zerotimestamp).total_seconds())
        
        #RUN THE LOOP FOR SEARCHING FOR NEXT 6 MONTHS i.e. 180 days FROM NOW
        for day_counter in range(1,MAX_DAYS):
            
            #get the date to be inserted into the query
            traveldateobject = date.today() + timedelta(days = day_counter)
            traveldatestring = unicode(traveldateobject)
            travelDate = traveldateobject.day
            travelMonth = traveldateobject.month
            travelYear = traveldateobject.year
            travelDay = traveldateobject.isoweekday()
            zerotimestamp2 = date.fromtimestamp(0)
            travelTimestamp = int((traveldateobject - zerotimestamp2).total_seconds())
            
            #MAIN BUSINESS : GENERATE THE URL and GET RESULTS
            sourceUrl = urldict[fromcity]
            destinationUrl = urldict[tocity]
            
            #RUN ANOTHER LOOP FOR THE NUMBER OF SEATS WE ARE LOOKING FOR (from 1 to 4)
            for num_seats in range(1,MAX_SEATS):
                
                #FORMULATE THE SEARCH QUERY
                search_query = 'http://uk.megabus.com/JourneyResults.aspx?originCode=' + urldict[fromcity] + '&destinationCode=' + urldict[tocity] + '&outboundDepartureDate=' + "{0:0=2d}".format(travelDate) + '%2f' + unicode(travelMonth) + '%2f' + unicode(travelYear) + '&inboundDepartureDate=&passengerCount=' + unicode(num_seats) + '&transportType=-1'

                page = requests.get(search_query)

                #SOUP EVERYTHING IN!!
                soup = BeautifulSoup(page.text)                
                #INITIALIZE AVAIL
                avail = 0
                price = INF
                #SEARCH FOR THE TODAY's MINIMUM PRICE IN HTML PAGE
                for i in soup.find_all('ul'):
                    if 'id' in i.attrs:
                         if (("JourneyResylts_OutboundList_GridViewResults" in i['id']) and ("row_item" in i['id'])):
                                ul_object = i
                                for ul_iterator in ul_object.find_all('li'):
                                    if 'class' in ul_iterator.attrs:
                                        if ("five" in ul_iterator['class']):
                                            li_object = ul_iterator
                                            price_string = unicode(li_object.p.string).split()[0].encode('utf-8')
                                            price_string = price_string.split('.')
                                            pre_decimal = re.search(r"\d+",price_string[0]).group()
                                            post_decimal = re.search(r"\d+",price_string[1]).group()
                                            price_num = int(pre_decimal) + float(post_decimal)/100
                                            if price_num < price:
                                                price = price_num
                                            avail = avail + 1
               
                if (avail == 0):
                    #WE COULD ALSO PUT THE PRIC HERE AS NaN
                    price = 0
                    

        
                #=============WRITE THESE THINGS TO THE JSON FILE==============
                if (for_comma==1):
                    fout.write('\n,\n')
                json.dump(OrderedDict([("resultAvailability",avail), ("resultPrice",price), 
                ("seatsLeft" , num_seats), ("sourceName",sourceName),("sourceLatitude",sourceLatitude),
                ("sourceLongitude",sourceLongitude), ("destinationname",destinationName), 
                ("destinationLatitude",destinationLatitude),("destinationLongitude",destinationLongitude),
                ("searchDate",searchDate), ("searchMonth",searchMonth), ("searchYear",searchYear),
                ("searchDay",searchDay), ("searchTimestamp",searchTimestamp), ("travelDay", travelDay),
                ("travelMonth", travelMonth), ("travelDate", travelDate), ("travelYear", travelYear),
                ("travelTimestamp",travelTimestamp)]), fout)
                #also append a comma with nextlines
                for_comma = 1
                
                
#add the ending ']' to the json file
fout.write('\n]')
#FINALLY, CLOSE THE JSON FILE
fout.close()                